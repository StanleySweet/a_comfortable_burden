using Godot;

namespace otter_ways.cm.ui
{
	public class Gauge : ProgressBar
	{
		//[Export] general.Timer _Timer;
		[Export] systems.MentalLoad _MentalLoad;

		public override void
		_Ready()
		{
			_MentalLoad.Connect(nameof(systems.MentalLoad.ValueChanged), this, nameof(OnTimerValueChanged));
			Value = 0d;
		}

		void
		OnTimerValueChanged(float newvalue)
		{
			Value = (newvalue / _MentalLoad.MaximumLoad) * MaxValue;
		}
	}
}
