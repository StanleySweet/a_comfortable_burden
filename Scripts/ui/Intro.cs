using Godot;

namespace otter_ways.cm.ui
{
	public class Intro : Node
	{
		[Export] NodePath _AnimationPlayerPath;
		[Export] NodePath _VideoPlayerPath;

		const string _ANIMATION_FINISHED = "animation_finished";
		const string _FINISHED = "finished";
		const string _TITLE_SCREEN_SCENE = "res://Scenes/UI/TitleScreen.tscn";
		Error _LoadingSceneResult;

		AnimationPlayer _AnimationPlayer;
		VideoPlayer _VideoPlayer;

		public override void
		_EnterTree()
		{
			_AnimationPlayer = GetNode<AnimationPlayer>(_AnimationPlayerPath);
			_VideoPlayer = GetNode<VideoPlayer>(_VideoPlayerPath);
			_ = _AnimationPlayer.Connect(_ANIMATION_FINISHED, this, nameof(OnAnimationPlayerFinished));
		}

		void
		OnAnimationPlayerFinished(string anim_name)
		{
			_AnimationPlayer.Disconnect(_ANIMATION_FINISHED, this, nameof(OnAnimationPlayerFinished));
			_ = _VideoPlayer.Connect(_FINISHED, this, nameof(OnVideoPlayerFinished));
			_VideoPlayer.Show();
			_VideoPlayer.Play();
		}

		void
		OnVideoPlayerFinished()
		{
			_VideoPlayer.Disconnect(_FINISHED, this, nameof(OnVideoPlayerFinished));
			_VideoPlayer.Hide();
			LoadTitleScreen();
		}

		void
		LoadTitleScreen()
		{
			_LoadingSceneResult = GetTree().ChangeScene(_TITLE_SCREEN_SCENE);

			if (_LoadingSceneResult == Error.CantOpen)
			{
				//>(Blanche) TODO: the path cannot be loaded into a PackedScene
			}
			else if (_LoadingSceneResult == Error.CantCreate)
			{
				//>(Blanche) TODO: that scene cannot be instantiated
			}
		}
	}
}
