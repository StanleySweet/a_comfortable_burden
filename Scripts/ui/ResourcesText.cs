using Godot;

namespace otter_ways.cm.ui
{
	public class ResourcesText : Control
	{
		[Export] NodePath _ResourcesPath;
		[Export] NodePath _RichTextLabelFRPath;
		[Export] NodePath _RichTextLabelENPath;

		Control _Resources;
		RichTextLabel _RichTextLabelFR;
		RichTextLabel _RichTextLabelEN;

		public override void
		_EnterTree()
		{
			_Resources = GetNode<Control>(_ResourcesPath);
			_RichTextLabelEN = GetNode<RichTextLabel>(_RichTextLabelENPath);
			_RichTextLabelFR = GetNode<RichTextLabel>(_RichTextLabelFRPath);
		}

		void
		OnResourcesVisibilityChanged()
		{
			if (_Resources.Visible)
			{
				switch (TranslationServer.GetLocale())
				{
					case "fr":
						_RichTextLabelEN.Hide();
						_RichTextLabelFR.Show();
						break;
					case "en":
						_RichTextLabelFR.Hide();
						_RichTextLabelEN.Show();
						break;
				}
			}
		}
	}
}
