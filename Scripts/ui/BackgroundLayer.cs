using Godot;

namespace otter_ways.cm.ui
{
	public class BackgroundLayer : Node
	{
		[Export] general.Timer _Timer;
		[Export] NodePath _DayBackgroundPath;
		[Export] NodePath _NightBackgroundPath;
		
		CanvasItem _DayBackground;
		CanvasItem _NightBackground;

		float _TimeToChange;
		bool _Changed;

		public override void
		_EnterTree()
		{
			_DayBackground = GetNode<CanvasItem>(_DayBackgroundPath);
			_NightBackground = GetNode<CanvasItem>(_NightBackgroundPath);
			_ = _Timer.Connect(nameof(general.Timer.Setted), this, nameof(OnTimerSetted));
			_ = _Timer.Connect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
		}

		public override void
		_ExitTree()
		{
			_Timer.Disconnect(nameof(general.Timer.Setted), this, nameof(OnTimerSetted));
			_Timer.Disconnect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
		}

		void
		OnTimerValueChanged(float newvalue)
        {
			if (!_Changed && newvalue <= _TimeToChange)
            {
				_DayBackground.Hide();
				_NightBackground.Show();
				_Changed = true;
            }
        }

		void
		OnTimerSetted()
		{
			_Changed = false;
			_TimeToChange = _Timer.Goal * .33f;
			_NightBackground.Hide();
			_DayBackground.Show();
		}
	}
}
