using Godot;

namespace otter_ways.cm.ui
{
	public class TitleScreen : TextureRect
	{
		//>(Blanche) TODO: Check buttons' paths have been set in the inspector
		[Export] NodePath _FirstButtonPath;
		[Export] NodePath _PressStartPath;
		[Export] NodePath _MenuContainerPath;
		[Export] NodePath _AnimationPlayerPath;
		[Export] Texture _Locked;
		[Export] Texture _Unlocked;

		const string _START = "start";
		const string _START_PRESSED = "start_pressed";
		const string _ACB_SCENE = "res://Scenes/ChargeMentale.tscn";
		Error _LoadingSceneResult;

		Button _FirstButton;
		Control _PressStart;
		Control _MenuContainer;
		AnimationPlayer _AnimationPlayer;

		bool _StartPressed;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_PressStart = GetNode<Control>(_PressStartPath);
			_MenuContainer = GetNode<Control>(_MenuContainerPath);
			_AnimationPlayer = GetNode<AnimationPlayer>(_AnimationPlayerPath);
		}

		public override void
		_Input(InputEvent @event)
		{
			if (Input.IsActionJustPressed(InputAxes.CANCEL))
			{
				if (_MenuContainer.Visible && _StartPressed)
				{
					OnCancelPressed();
					GetTree().SetInputAsHandled();
				}
			}
			if (Input.IsActionJustPressed(InputAxes.START_SCREEN))
			{
				if (!_StartPressed)
				{
					OnPressStartPressed();
					GetTree().SetInputAsHandled();
				}
			}
		}

		void
		OnPressStartPressed()
		{
			_StartPressed = true;
			Texture = _Unlocked;
			_MenuContainer.Show();
			_AnimationPlayer.Play(_START_PRESSED);
			_PressStart.Hide();
			_FirstButton.GrabFocus();
		}

		void
		OnCancelPressed()
		{
			_StartPressed = false;
			Texture = _Locked;
			_MenuContainer.Hide();
			_MenuContainer.Modulate = new Color(_MenuContainer.Modulate.r, _MenuContainer.Modulate.g,
												_MenuContainer.Modulate.b, 0f);
			_AnimationPlayer.Play(_START);
			_PressStart.Show();
		}

		void
		OnPlayPressed()
		{
			_LoadingSceneResult = GetTree().ChangeScene(_ACB_SCENE);

			if (_LoadingSceneResult == Error.CantOpen)
			{
				//>(Blanche) TODO: the path cannot be loaded into a PackedScene
			}
			else if (_LoadingSceneResult == Error.CantCreate)
			{
				//>(Blanche) TODO: that scene cannot be instantiated
			}
		}

		void
		OnSettingsPressed()
		{
			_MenuContainer.Hide();
		}

		void
		OnResourcesPressed()
		{
			_MenuContainer.Hide();
		}

		void
		OnCreditsPressed()
		{
			_MenuContainer.Hide();
		}

		void
		OnExitPressed()
		{
			GetTree().Quit();
		}

		void
		OnSettingsMenuClosing()
		{
			_MenuContainer.Show();
			_FirstButton.GrabFocus();
		}

		void
		OnResourcesMenuClosing()
		{
			_MenuContainer.Show();
			_FirstButton.GrabFocus();
		}

		void
		OnCreditsMenuClosing()
		{
			_MenuContainer.Show();
			_FirstButton.GrabFocus();
		}
	}
}
