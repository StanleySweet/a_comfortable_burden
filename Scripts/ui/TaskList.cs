using Godot;

namespace otter_ways.cm.ui
{
	public class TaskList : Control
	{
		[Export] systems.GameRules _GameRules;
		[Export] systems.TaskList _TaskList;
		[Export] NodePath _TasksContainerPath;
		//[Export] NodePath _FirstChildPath;
		[Export] PackedScene _TaskLabelScene;

		System.Collections.Generic.List<Label> _TasksLabels;
		Label _LabelInstance;
		Control _TasksContainer;
		//Node _FirstChild;

		public override void
		_EnterTree()
		{
			_TasksLabels = new System.Collections.Generic.List<Label>();
			_TasksContainer = GetNode<Control>(_TasksContainerPath);
			//_FirstChild= GetNode<Node>(_FirstChildPath);
			_ = _TaskList.Connect(nameof(systems.TaskList.Resetting), this, nameof(OnTaskListResetting));
		}

		public override void
		_ExitTree()
		{
			_TaskList.Disconnect(nameof(systems.TaskList.Resetting), this, nameof(OnTaskListResetting));
		}

		void
		OnTaskListResetting()
		{
			if (_GameRules.CurrentGameMode == systems.GameMode.Man)
			{
				Hide();

				for (int i = 0; i < _TaskList.Tasks.Count; i++)
				{
					_TaskList.Tasks[i].Disconnect(nameof(actors.items.task.Task.Completed), this, 
												  nameof(OnTaskCompleted));
				}

				for (int i = 0; i < _TasksLabels.Count; i++)
				{
					if (!_TasksLabels[i].IsQueuedForDeletion())
					{
						_TasksLabels[i].QueueFree();
					}
				}
				_TasksLabels.Clear();
			}
		}

		//void
		//OnTaskRevealed(int id)
		//{
		//	_TaskList.Tasks[id].Disconnect(nameof(actors.items.task.Task.Revealed), this, nameof(OnTaskRevealed));

		//	_LabelInstance = (Label)_TaskLabelScene.Instance();
		//	_LabelInstance.Text = _TaskList.Tasks[id].TextToDisplay;
		//	_TasksLabels[id] = _LabelInstance;
		//	_TasksContainer.AddChildBelowNode(_FirstChild, _LabelInstance);
		//	_LabelInstance = null;
		//}

		void
		OnTaskScreenGOPressed()
		{
			for (int i = 0; i < _TaskList.Tasks.Count; i++)
			{
				_TaskList.Tasks[i].Connect(nameof(actors.items.task.Task.Completed), this, nameof(OnTaskCompleted));

				//if (_TaskList.Tasks[i].StartRevealed)
				//{
					_LabelInstance = (Label)_TaskLabelScene.Instance();
					_LabelInstance.Text = _TaskList.Tasks[i].TextToDisplay;
					_TasksLabels.Add(_LabelInstance);
					_TasksContainer.AddChild(_LabelInstance);
					_LabelInstance = null;
				//}
				//else
				//{
				//	_TasksLabels.Add(null);
				//	_TaskList.Tasks[i].Connect(nameof(actors.items.task.Task.Revealed), this, nameof(OnTaskRevealed));
				//}
			}

			Show();
		}

		void
		OnTaskCompleted(int id)
		{
			_TasksLabels[id].Modulate = Color.Color8(160, 160, 160);
		}
	}
}
