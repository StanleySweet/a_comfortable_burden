using Godot;

namespace otter_ways.cm.ui
{
	public class ResourcesMenu : Control
	{
		[Signal] public delegate void Closing();

		[Export] NodePath _FirstButtonPath;
		[Export] NodePath _RichTextLabelFRPath;
		[Export] NodePath _RichTextLabelENPath;

		RichTextLabel _RichTextLabelFR;
		RichTextLabel _RichTextLabelEN;
		Button _FirstButton;
		bool _CancelPressed;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_RichTextLabelEN = GetNode<RichTextLabel>(_RichTextLabelENPath);
			_RichTextLabelFR = GetNode<RichTextLabel>(_RichTextLabelFRPath);
		}

		public override void
		_Ready()
		{
			SetProcessInput(false);
		}

		public override void
		_Input(InputEvent @event)
		{
			if (Input.IsActionJustPressed(InputAxes.CANCEL))
			{
				if (!_CancelPressed)
				{
					OnBackPressed();
					GetTree().SetInputAsHandled();
				}
			}
		}

		void
		OnBackPressed()
		{
			_CancelPressed = true;
			Hide();
			SetProcessInput(false);
			EmitSignal(nameof(Closing));
		}

		void
		OnUIRequestResources()
		{
			_CancelPressed = false;

			string currentLocale = TranslationServer.GetLocale().Substring(0, 2).ToLower();

			switch (currentLocale)
			{
				case "fr":
					_RichTextLabelEN.Hide();
					_RichTextLabelFR.Show();
					break;
				case "en":
					_RichTextLabelFR.Hide();
					_RichTextLabelEN.Show();
					break;
			}

			Show();
			SetProcessInput(true);
			_FirstButton.GrabFocus();
		}
	}
}
