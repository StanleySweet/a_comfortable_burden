using Godot;

namespace otter_ways.cm.ui
{
	public class VictoryMenu : Control
	{
		[Signal] public delegate void RequestNextLevel();
		[Signal] public delegate void RequestTitleScreen();

		[Export] systems.GameRules _GameRules;
		//>(Blanche) TODO: Check _FirstButtonPath has been set in the inspector
		[Export] NodePath _FirstButtonPath;

		Button _FirstButton;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_ = _GameRules.Connect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
		}

		public override void 
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
		}

		void
		OnGameRulesVictory()
		{
			Show();
			_FirstButton.GrabFocus();
		}

		void
		OnNextLevelPressed()
		{
			Hide();
			EmitSignal(nameof(RequestNextLevel));
		}

		void
		OnTitleScreenPressed()
		{
			//>(Blanche) NOTE: Every scenes opened will be unloaded then the title screen scene will be loaded, it's useless to clean before
			EmitSignal(nameof(RequestTitleScreen));
		}
	}
}
