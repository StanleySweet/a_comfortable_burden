using Godot;

namespace otter_ways.cm.ui
{
	public class Timer : Label
	{
		[Export] general.Timer _Timer;
		//[Export] readonly int _DEFAULT_TEXT_SIZE;
		//[Export] CurveTexture _WorryingTextCurve;
		//[Export] readonly int _WORRYING_TEXT_SIZE_END;
		//[Export] readonly float _WORRYING_TEXT_SIZE_TIME = 15f;
		//[Export] CurveTexture _CriticalTextCurve;
		//[Export] readonly int _CRITICAL_TEXT_SIZE_END;
		//[Export] readonly float _CRITICAL_TEXT_SIZE_TIME = 15f;

		//DynamicFont _DynamicFont;
		//int _NewSize;
		//float _CurrentLerpTime;

		//float _TimeToWorrying;
		//float _TimeToCritical;
		//bool _PlayWorrying;
		//bool _PlayCritical;
		//bool _PlayedWorrying;
		//bool _PlayedCritical;

		public override void
		_EnterTree()
		{
			//_DynamicFont = (DynamicFont)GetFont("font");
			_ = _Timer.Connect(nameof(general.Timer.Setted), this, nameof(OnTimerSetted));
			_ = _Timer.Connect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
		}

        public override void 
		_ExitTree()
        {
			_Timer.Disconnect(nameof(general.Timer.Setted), this, nameof(OnTimerSetted));
			_Timer.Disconnect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
        }

  //      public override void 
		//_Ready()
  //      {
		//	SetProcess(false);
  //      }

  //      public override void 
		//_Process(float delta)
  //      {
		//	if (_PlayWorrying)
		//	{
		//		_CurrentLerpTime += delta;
		//		if (_CurrentLerpTime >= _WORRYING_TEXT_SIZE_TIME)
		//		{
		//			_NewSize = _WORRYING_TEXT_SIZE_END;
		//			_PlayWorrying = false;
		//			_CurrentLerpTime = 0f;
		//		}
		//		else
		//		{
  //                  _NewSize = 
		//				(int)_WorryingTextCurve.Curve.Interpolate(_CurrentLerpTime / _WORRYING_TEXT_SIZE_TIME);
		//		}
		//		if (_DynamicFont.Size != _NewSize)
		//		{
		//			_DynamicFont.Size = _NewSize;
		//			AddFontOverride("font", _DynamicFont);
		//		}
		//	}
		//	if (_PlayCritical)
		//	{
		//		_CurrentLerpTime += delta;
		//		if (_CurrentLerpTime >= _CRITICAL_TEXT_SIZE_TIME)
		//		{
		//			_NewSize = _CRITICAL_TEXT_SIZE_END;
		//			_PlayCritical = false;
		//			_CurrentLerpTime = 0f;
		//			SetProcess(false);
		//		}
		//		else
		//		{
		//			_NewSize = 
		//				(int)_CriticalTextCurve.Curve.Interpolate(_CurrentLerpTime / _CRITICAL_TEXT_SIZE_TIME);
		//		}
		//		if (_DynamicFont.Size != _NewSize)
  //              {
		//			_DynamicFont.Size = _NewSize;
		//			AddFontOverride("font", _DynamicFont);
  //              }
		//	}
		//}

        void
		OnTimerValueChanged(float newvalue)
		{
			//if (!_PlayedWorrying && newvalue <= _TimeToWorrying)
   //         {
			//	_PlayedWorrying = true;
			//	_PlayWorrying = true;
			//	SetProcess(true);
			//}
			//if (!_PlayedCritical && newvalue <= _TimeToCritical)
			//{
			//	_PlayedCritical	= true;
			//	_PlayCritical = true;
			//}
			Text = string.Format("{0:00}", (int)newvalue);
		}

		void
		OnTimerSetted()
		{
			//_DEFAULT_TEXT_SIZE;
			//_CurrentLerpTime = 0f;
			//SetProcess(false);
			//_PlayCritical = false;
			//_PlayWorrying = false;
			//_PlayedWorrying = false;
			//_PlayedCritical = false;
			//_TimeToWorrying = 30f;
			//_TimeToCritical = 15f;
			Text = string.Format("{0:00}", (int)_Timer.Goal);
		}
	}
}
