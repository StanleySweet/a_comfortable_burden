using Godot;

namespace otter_ways.cm.ui
{
	public class DefeatMenu : Control
	{
		[Signal] public delegate void RequestLevelRestart();
		[Signal] public delegate void RequestTitleScreen();

		[Export] systems.GameRules _GameRules;
		//>(Blanche) TODO: Check _FirstButtonPath has been set in the inspector
		[Export] NodePath _FirstButtonPath;

		Button _FirstButton;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_ = _GameRules.Connect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
		}

		public override void 
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
		}

		void
		OnGameRulesDefeat()
		{
			Show();
			_FirstButton.GrabFocus();
		}

		void
		OnRestartPressed()
		{
			Hide();
			EmitSignal(nameof(RequestLevelRestart));
		}

		void
		OnTitleScreenPressed()
		{
			Hide();
			//>(Blanche) NOTE: Every scenes opened will be unloaded then the title screen scene will be loaded, it's useless to clean before
			EmitSignal(nameof(RequestTitleScreen));
		}
	}
}
