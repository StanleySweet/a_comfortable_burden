using Godot;

namespace otter_ways.cm
{
	public class Director : Node
	{
		[Export] systems.Player _Player;
		[Export] general.Timer _TimerSystem;
		//[Export] systems.MentalLoad _MentalLoad;
		[Export] systems.GameRules _GameRules;
		[Export] systems.TaskList _TaskList;

		[Export] NodePath _LevelParentPath;
		[Export] NodePath _PauseMenuPath;
		[Export] NodePath _VideoPlayerPath;

		const string _FINISHED = "finished";
		const string _TITLE_SCREEN_SCENE = "res://Scenes/UI/TitleScreen.tscn";
		Error _LoadingSceneResult;

		SceneTree _CurrentTree;
		VideoPlayer _VideoPlayer;
		ui.PauseMenu _PauseMenu;
		bool _PauseMenuOpened;

		public override void
		_EnterTree()
		{
			_CurrentTree = GetTree();
			_Player.Initialise(_CurrentTree, _GameRules);
			_ = _Player.Connect(nameof(systems.Player.PressedPause), this, nameof(OnPlayerPressedPause));
			//_MentalLoad.Initialise();
			_TaskList.Initialise();
			_GameRules.Initialise(GetNode<Node>(_LevelParentPath));
			_ = _GameRules.Connect(nameof(systems.GameRules.CutsceneStarting), this, nameof(OnGameRulesCutsceneStarting));
			_PauseMenu = GetNode<ui.PauseMenu>(_PauseMenuPath);
			_VideoPlayer = GetNode<VideoPlayer>(_VideoPlayerPath);
		}

		public override void 
		_Ready()
		{
			_GameRules.GameStart();
		}

		public override void
		_ExitTree()
		{
			_CurrentTree.Paused = false;
			_CurrentTree = null;
			_GameRules.Reset();
			_GameRules.Disconnect(nameof(systems.GameRules.CutsceneStarting), this, nameof(OnGameRulesCutsceneStarting));
			_Player.Destroy();
			_Player.Disconnect(nameof(systems.Player.PressedPause), this, nameof(OnPlayerPressedPause));
		}

		public override void
		_PhysicsProcess(float delta)
		{
			if (!_PauseMenuOpened) 
			{
				_TimerSystem._PhysicsProcess(delta);
			}
			_Player._PhysicsProcess(delta);
		}

		public override void
		_UnhandledInput(InputEvent @event)
		{
			_Player._UnhandledInput(@event);
		}

		public void
		OnTaskScreenGOPressed()
		{
			_GameRules.LevelStart();
		}

		void
		OnUIRequestNextLevel()
		{
			_GameRules.LevelNext();
		}

		void
		OnDefeatMenuRequestLevelRestart()
		{
			_GameRules.LevelRestart();
		}

		void
		OnUIRequestTitleScreen()
		{
			if (_GameRules.Level10Running)
			{
				_GameRules.LevelNext();
				return;
			}

			_LoadingSceneResult = GetTree().ChangeScene(_TITLE_SCREEN_SCENE);

			if (_LoadingSceneResult == Error.CantOpen)
			{
				//>(Blanche) TODO: the path cannot be loaded into a PackedScene
			}
			else if (_LoadingSceneResult == Error.CantCreate)
			{
				//>(Blanche) TODO: that scene cannot be instantiated
			}
		}

		void
		OnUIRequestResume()
		{
			_PauseMenuOpened = false;
			_CurrentTree.Paused = false;
			_Player.ToGame();
		}

		void
		OnPlayerPressedPause()
		{
			if (!_PauseMenuOpened && !_CurrentTree.Paused)
			{
				_CurrentTree.Paused = true;
				_PauseMenuOpened = true;
				_PauseMenu.Open();
			}
			else if (_PauseMenuOpened)
			{
				_CurrentTree.Paused = false;
				_PauseMenuOpened = false;
				_PauseMenu.Close();
			}
		}

		void
		OnGameRulesCutsceneStarting()
		{
			_ = _VideoPlayer.Connect(_FINISHED, this, nameof(OnVideoPlayerFinished));
			_VideoPlayer.Show();
			_VideoPlayer.Play();
		}

		void
		OnVideoPlayerFinished()
		{
			_VideoPlayer.Disconnect(_FINISHED, this, nameof(OnVideoPlayerFinished));
			_VideoPlayer.Hide();
			_GameRules.CutsceneFinished();
		}
	}
}
