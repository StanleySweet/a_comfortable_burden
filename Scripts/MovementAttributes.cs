using Godot;

namespace otter_ways.cm
{
    public class MovementAttributes : Resource
	{
		[Export] float _Speed = 280f;
		public float Speed { get { return _Speed; } }
		[Export] float _AttackTime = .180f;
		public float AttackTime { get { return _AttackTime; } }
		[Export] CurveTexture _Attack;
		public CurveTexture Attack { get { return _Attack; } }
		[Export] float _ReleaseTime = .160f;
		public float ReleaseTime { get { return _ReleaseTime; } }
		[Export] CurveTexture _Release;
		public CurveTexture Release { get { return _Release; } }
	}
}
