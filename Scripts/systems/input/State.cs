namespace otter_ways.cm.systems.input
{
    public abstract class State
    {
        protected Player _Player;

        public
        State(Player player)
        {
            _Player = player;
        }

        public virtual void
        _UnhandledInput(Godot.InputEvent @event)
        { }

        public virtual void
        _PhysicsProcess(float delta)
        { }
    }
}
