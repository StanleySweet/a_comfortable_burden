using Godot;

namespace otter_ways.cm.systems
{
	public class GameRules : Resource
	{
		const int _NUMBER_OF_LEVELS = 15;
		const string _TREE_EXITED = "tree_exited";

		[Signal] public delegate void LevelStarting();
		[Signal] public delegate void CutsceneStarting();
		[Signal] public delegate void ChangeController();
		[Signal] public delegate void Victory();
		[Signal] public delegate void Defeat();
		[Signal] public delegate void GameEnd();

        [Export] general.Timer _Timer;
        //[Export] MentalLoad _MentalLoad;
		[Export] TaskList _TaskList;
		[Export] LevelConfiguration[] _LevelsConfiguration = new LevelConfiguration[_NUMBER_OF_LEVELS];

        Node _LevelsParent;
		Node _CurrentLevelInstance;
		public GameMode CurrentGameMode { get; private set; }
		int _CurrentLevel;
		public bool Level10Running { get; private set; }
		bool _Loop;
		bool _ShouldLoadNextLevel;
		bool _ComingBackFromCutscene;
		bool _DefeatScreenDisplayed;

		public void
		Initialise(Node levels_parent)
		{
			_LevelsParent = levels_parent;
            _ = _Timer.Connect(nameof(general.Timer.Ended), this, nameof(OnTimerEnded));
            //_ = _MentalLoad.Connect(nameof(MentalLoad.Full), this, nameof(OnTimerEnded));
            _ = _TaskList.Connect(nameof(TaskList.Completed), this, nameof(OnTaskListCompleted));
		}

		public void
		Reset()
        {
			_LevelsParent = null;
			_CurrentLevelInstance = null;
			_CurrentLevel = 0;
			Level10Running = false;
			_DefeatScreenDisplayed = false;
			_Timer.Reset();
			//_MentalLoad.Reset();
			_TaskList.Reset();
			_Timer.Disconnect(nameof(general.Timer.Ended), this, nameof(OnTimerEnded));
			//_MentalLoad.Disconnect(nameof(MentalLoad.Full), this, nameof(OnTimerEnded));
			_TaskList.Disconnect(nameof(TaskList.Completed), this, nameof(OnTaskListCompleted));
		}

		public void
		CutsceneFinished()
        {
			_ComingBackFromCutscene = true;
			OnCurrentLevelTreeExited();
        }

		void
		OnTaskListCompleted()
		{
			if (!_DefeatScreenDisplayed)
            {
				_Timer.Stop();
				if (_CurrentLevel == 14)
                {
					EmitSignal(nameof(GameEnd));
                }
				else
                {
					EmitSignal(nameof(Victory));
                }
            }
		}

		void
		OnTimerEnded()
		{
			if (_Loop)
            {
				_Loop = false;
				EmitSignal(nameof(ChangeController));
				_Timer.Reset();
                //_MentalLoad.Reset();
                _Timer.Set(_LevelsConfiguration[_CurrentLevel].TimerDuration);
                _Timer.Start();
                return;
            }
            else
            {
				EmitSignal(nameof(Defeat));
				_DefeatScreenDisplayed = true;
            }
		}

		void
		OnCurrentLevelTreeExited()
        {
			if (_ShouldLoadNextLevel)
			{
				_CurrentLevel++;
				CurrentGameMode = _LevelsConfiguration[_CurrentLevel].GameMode;

				if (_CurrentLevel == 10)
                {
					Level10Running = true;
                }
				if (_CurrentLevel == 11)
				{
					Level10Running = false;
					EmitSignal(nameof(CutsceneStarting));
					_ShouldLoadNextLevel = false;
					return;
				}
			}

			if (!_ComingBackFromCutscene)
            {
				_CurrentLevelInstance.Disconnect(_TREE_EXITED, this, nameof(OnCurrentLevelTreeExited));
            }
			else
            {
				_ComingBackFromCutscene = false;
            }

			_CurrentLevelInstance = _LevelsConfiguration[_CurrentLevel].Scene.Instance();
			if (Level10Running)
            {
				(_CurrentLevelInstance as Level10).Initialise(this, _Timer);
            }
			_LevelsParent.AddChild(_CurrentLevelInstance);

			_Loop = CurrentGameMode == GameMode.Both;
			_Timer.Set(_LevelsConfiguration[_CurrentLevel].TimerDuration);
			_TaskList.Ready();
			if (CurrentGameMode != GameMode.Man)
			{
				LevelStart();
			}

			_ShouldLoadNextLevel = false;
		}

		public void
		GameStart()
		{
			_CurrentLevelInstance = _LevelsConfiguration[0].Scene.Instance();
			_LevelsParent.AddChild(_CurrentLevelInstance);
			CurrentGameMode = _LevelsConfiguration[0].GameMode;
			_Loop = CurrentGameMode == GameMode.Both;
            _Timer.Set(_LevelsConfiguration[0].TimerDuration);
            _TaskList.Ready();
		}

		public void
		LevelStart()
		{
			EmitSignal(nameof(LevelStarting));
            _Timer.Start();
        }

		public void
		LevelRestart()
		{
			if (Level10Running)
            {
				LevelNext();
				return;
            }

			_DefeatScreenDisplayed = false;
			_Timer.Reset();
            //_MentalLoad.Reset();
			_TaskList.Reset();

			_ = _CurrentLevelInstance.Connect(_TREE_EXITED, this, nameof(OnCurrentLevelTreeExited));
            _CurrentLevelInstance.QueueFree();
		}

		public void
		LevelNext()
		{
			_DefeatScreenDisplayed = false;
            _Timer.Reset();
			//_MentalLoad.Reset();
			_TaskList.Reset();

			_ShouldLoadNextLevel = true;
			_ = _CurrentLevelInstance.Connect(_TREE_EXITED, this, nameof(OnCurrentLevelTreeExited));
			_CurrentLevelInstance.QueueFree();
		}
	}
}
