using Godot;

namespace otter_ways.general
{
	//>(Blanche) INFO : Transformer cette classe plus tard pour gerer plusieurs timers en parallele
	public class Timer : Resource
	{
		[Signal] public delegate void Setted();
		[Signal] public delegate void Ended();
		[Signal] public delegate void ValueChanged(float newvalue);

		float _Remaining;
		public float Goal { get; private set; }

		bool _Running;

		public void
		_PhysicsProcess(float delta)
		{
			if (_Running)
			{
				_Remaining -= delta;
				EmitSignal(nameof(ValueChanged), _Remaining);

				if (_Remaining <= 0f)
				{
					_Running = false;
					_Remaining = 0f;
					EmitSignal(nameof(Ended));
				}
			}
		}

		public void 
		Start()
		{
			_Running = true;
		}

		public void
		Stop()
		{
			_Running = false;
		}

		public void
		Set(float goal)
		{
			_Remaining = Goal = goal;
			EmitSignal(nameof(Setted));
		}

		public void
		Reset()
		{
			_Running = false;
			_Remaining = Goal = 0f;
		}
	}
}
