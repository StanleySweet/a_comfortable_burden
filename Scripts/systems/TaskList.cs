using Godot;
using System.Collections.Generic;

namespace otter_ways.cm.systems
{
	public class TaskList : Resource
	{
		[Signal] public delegate void Completed();
		[Signal] public delegate void Readied();
		[Signal] public delegate void Resetting();

		public List<actors.items.task.thing> Tasks { get; private set; }

		int _Count;

		public void
		Ready()
		{
			EmitSignal(nameof(Readied));
		}

		public void
		Initialise()
		{
			Tasks = new List<actors.items.task.thing>();
		}

		public void
		Reset()
		{
			EmitSignal(nameof(Resetting));
			for (int i = 0; i < Tasks.Count; i++)
			{
				Tasks[i].Disconnect(nameof(actors.items.task.Task.Completed), this, nameof(OnTaskCompleted));
			}
			Tasks.Clear();
			_Count = 0;
		}

		public int
		Register(actors.items.task.thing toregister)
		{
			toregister.Connect(nameof(actors.items.task.Task.Completed), this, nameof(OnTaskCompleted));
			Tasks.Add(toregister);
			_Count++;

			return Tasks.Count - 1;
		}

		void
		OnTaskCompleted(int id)
		{
			_Count--;
			if (_Count == 0)
			{
				EmitSignal(nameof(Completed));
			}
		}
	}
}
