using Godot;

namespace otter_ways.cm.systems
{
	public class Player : Resource
	{
		[Signal] public delegate void PressedPause();
		[Signal] public delegate void ControllerChanged();

		[Export] ARAttributes _AttackRelease;

		GameRules _GameRules;
		SceneTree _CurrentTree;

		public actors.playable.Controller CurrentController { get; private set; }
		actors.playable.Controller _Man;
		actors.playable.Controller _Woman;

		input.State _CurrentState;
		input.Disabled _CachedDisabled;
		input.Game _CachedGame;
        input.PauseMenu _CachedPauseMenu;

        public ARAttributes AttackRelease { get { return _AttackRelease; } }

		public void
		Initialise(SceneTree currenttree, GameRules gamerules)
		{
			_CachedDisabled = new input.Disabled(this);
			_CachedPauseMenu = new input.PauseMenu(this);
			_CachedGame = new input.Game(this);
			_CurrentState = _CachedDisabled;

			_CurrentTree = currenttree;

			_GameRules = gamerules;
			_ = _GameRules.Connect(nameof(GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_ = _GameRules.Connect(nameof(GameRules.CutsceneStarting), this, nameof(OnGameRulesCutsceneStarting));
			_ = _GameRules.Connect(nameof(GameRules.ChangeController), this, nameof(OnGameRulesChangeController));
			_ = _GameRules.Connect(nameof(GameRules.Victory), this, nameof(OnGameRulesVictory));
			_ = _GameRules.Connect(nameof(GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_ = _GameRules.Connect(nameof(GameRules.GameEnd), this, nameof(OnGameRulesVictory));
		}

		public void
		Destroy()
		{
			_GameRules.Disconnect(nameof(GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_GameRules.Disconnect(nameof(GameRules.CutsceneStarting), this, nameof(OnGameRulesCutsceneStarting));
			_GameRules.Disconnect(nameof(GameRules.ChangeController), this, nameof(OnGameRulesChangeController));
			_GameRules.Disconnect(nameof(GameRules.Victory), this, nameof(OnGameRulesVictory));
			_GameRules.Disconnect(nameof(GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_GameRules.Disconnect(nameof(GameRules.GameEnd), this, nameof(OnGameRulesVictory));
			_GameRules = null;
		}

		public void
		_UnhandledInput(InputEvent @event)
		{
			_CurrentState._UnhandledInput(@event);
			_CurrentTree.SetInputAsHandled();
		}

		public void
		_PhysicsProcess(float delta)
		{
			_CurrentState._PhysicsProcess(delta);
		}

		void
		OnGameRulesLevelStarting()
		{
			CurrentController = _GameRules.CurrentGameMode == GameMode.Man ? _Man : _Woman;
			CurrentController.TransitionIn();
			EmitSignal(nameof(ControllerChanged));

			ToGame();
		}

		void
		OnGameRulesCutsceneStarting()
        {
			ToDisabled();
        }

		void
		OnGameRulesChangeController()
        {
			_CachedGame.PutOnHold();
			CurrentController.TransitionOut();
			CurrentController = _Man;
			CurrentController.TransitionIn();
			EmitSignal(nameof(ControllerChanged));
		}

		void
		OnGameRulesVictory()
		{
			_CachedGame.PutOnHold();
			ResetControllers();
			ToDisabled();
		}

		void
		OnGameRulesDefeat()
		{
			_CachedGame.PutOnHold();
			ResetControllers();
			ToDisabled();
		}

		public void
		EmitPressedPause()
        {
			EmitSignal(nameof(PressedPause));
		}

		public void
		ToDisabled()
		{
			_CurrentState = _CachedDisabled;
		}

		public void
		ToPauseMenu()
        {
            _CurrentState = _CachedPauseMenu;
        }

		public void
		ToGame()
        {
			_CurrentState = _CachedGame;
		}

		public void
		AttachController(actors.playable.Controller controller)
		{
			if (controller.Identity == actors.playable.Existing.Man)
			{
				_Man = controller;
			}
			else
			{
				_Woman = controller;
			}
		}

		void
		ResetControllers()
		{
			CurrentController = _Man = _Woman = null;
		}
	}
}
