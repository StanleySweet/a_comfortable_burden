using Godot;

namespace otter_ways.cm
{
	public class Level10 : Node
	{
		const float _VIGNETTE_START = 20f;
		const float _SECOND_PASS_1ST_SHAKE_START = 12f;
		const float _SECOND_PASS_2ND_SHAKE_START = 7f;
		const float _SECOND_PASS_3RD_SHAKE_START = 4f;

		[Export] NodePath _VignettePath;
		[Export] NodePath _CameraRigPath;
		[Export] CameraShakeInfo[] _SecondPassShakes;
		[Export] CurveTexture _VignetteCurve;
		[Export] readonly float _VIGNETTE_TIME = 15f;

		systems.GameRules _GameRules;
		general.Timer _Timer;
		CameraRig _CameraRig;
		Sprite _Vignette;

		bool _PlayedVignette;
		bool _SecondPass1stShakeDone;
		bool _SecondPass2ndShakeDone;
		bool _SecondPass3rdShakeDone;

		float _CurrentLerpTime;

		public void
		Initialise(systems.GameRules gamerules, general.Timer timer)
		{
			_GameRules = gamerules;
			_Timer = timer;
			_ = _GameRules.Connect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_ = _Timer.Connect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
		}

		public override void
		_EnterTree()
		{
			_Vignette = GetNode<Sprite>(_VignettePath);
			_CameraRig = GetNode<CameraRig>(_CameraRigPath);
		}

		public override void
		_Ready()
		{
			SetProcess(false);
		}

		public override void
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_Timer.Disconnect(nameof(general.Timer.ValueChanged), this, nameof(OnTimerValueChanged));
		}

		public override void 
		_Process(float delta)
		{
			_CurrentLerpTime += delta;
			if (_CurrentLerpTime >= _VIGNETTE_TIME)
			{
				_Vignette.Modulate = new Color(_Vignette.Modulate.r, _Vignette.Modulate.g,
											   _Vignette.Modulate.b, 1f);
				_CurrentLerpTime = 0f;
				SetProcess(false);
			}
			else
			{
				_Vignette.Modulate = new Color(_Vignette.Modulate.r, _Vignette.Modulate.g, _Vignette.Modulate.b, 
											   _VignetteCurve.Curve.Interpolate(_CurrentLerpTime / _VIGNETTE_TIME));
			}
		}

		void
		OnGameRulesDefeat()
		{
			_CameraRig.Reboot();
		}

		void
		OnTimerValueChanged(float newvalue)
		{
			if (!_PlayedVignette && newvalue <= _VIGNETTE_START)
			{
				_PlayedVignette = true;
				SetProcess(true);
			}
			if (!_SecondPass1stShakeDone && newvalue <= _SECOND_PASS_1ST_SHAKE_START)
			{
				_SecondPass1stShakeDone = true;
				_CameraRig.Shake(_SecondPassShakes[0]);
			}
			if (!_SecondPass2ndShakeDone && newvalue <= _SECOND_PASS_2ND_SHAKE_START)
			{
				_SecondPass2ndShakeDone = true;
				_CameraRig.Shake(_SecondPassShakes[1]);
			}
			if (!_SecondPass3rdShakeDone && newvalue <= _SECOND_PASS_3RD_SHAKE_START)
			{
				_SecondPass3rdShakeDone = true;
				_CameraRig.Shake(_SecondPassShakes[2]);
			}
		}
	}
}
