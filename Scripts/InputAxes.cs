namespace otter_ways.cm
{
	public static class InputAxes
	{
		public const string ACTION = "action";
		public const string PAUSE_MENU = "pause_menu";
		public const string VALIDATE = "validate";
		public const string CANCEL = "cancel";
		public const string ANALOG_LEFT = "analog_left";
		public const string ANALOG_RIGHT = "analog_right";
		public const string ANALOG_UP = "analog_up";
		public const string ANALOG_DOWN = "analog_down";
		public const string DIGITAL_LEFT = "digital_left";
		public const string DIGITAL_RIGHT = "digital_right";
		public const string DIGITAL_UP = "digital_up";
		public const string DIGITAL_DOWN = "digital_down";
		public const string START_SCREEN = "start_screen";
	}
}
