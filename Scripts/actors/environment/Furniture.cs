using Godot;

namespace otter_ways.cm.actors.environment
{
	public class Furniture : Actor
	{
		[Export] readonly bool _STARTING_HIDDEN;

		public override void
		_EnterTree()
		{
			base._EnterTree();
			if (_STARTING_HIDDEN)
			{
				Disappear();
			}
		}
	}
}
