using Godot;

namespace otter_ways.cm.actors.environment
{
	public class Wall : Actor
	{
		[Export] Texture _LoweredSprite;
		[Export] readonly bool _STARTING_LOWERED;

		Texture _DefaultSprite;

		public override void
		_EnterTree()
		{
			base._EnterTree();
			_DefaultSprite = _Sprite.Texture;
			if (_STARTING_LOWERED)
			{
				Disappear();
			}
		}

		public override void 
		Disappear()
		{
			_StayHidden = _Hidden ? true : false;
			_Hidden = true;
			if (!_StayHidden)
			{
				_Sprite.Texture = _LoweredSprite;
			}
		}

		public override void
		ResetSprite()
		{
			if (_StayHidden)
			{
				_StayHidden = false;
			}
			else
			{
				_Sprite.Texture = _DefaultSprite;
				_Hidden = false;
			}
		}
	}
}
