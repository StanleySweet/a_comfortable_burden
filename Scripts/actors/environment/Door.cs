using Godot;

namespace otter_ways.cm.actors.environment
{
	public class Door : Actor
	{
		[Export] Texture _LoweredSprite;
		[Export] readonly bool _STARTING_LOWERED;
		[Export] readonly bool _CORRECT_OFFSET;
		[Export] Vector2 _NewOffset;

		Texture _DefaultSprite;
		Vector2 _DefaultOffset;

		public override void
		_EnterTree()
		{
			base._EnterTree();
			_DefaultOffset = _Sprite.Offset;
			_DefaultSprite = _Sprite.Texture;
			if (_STARTING_LOWERED)
			{
				Disappear();
			}
		}

		public override void
		Disappear()
		{
			_StayHidden = _Hidden ? true : false;
			_Hidden = true;
			if (!_StayHidden)
			{
				_Sprite.Texture = _LoweredSprite;
				if (_CORRECT_OFFSET)
				{
					_Sprite.Offset = _NewOffset;
				}
			}
		}

		public override void
		ResetSprite()
		{
			if (_StayHidden)
			{
				_StayHidden = false;
			}
			else
			{
				_Sprite.Texture = _DefaultSprite;
				if (_CORRECT_OFFSET)
				{
					_Sprite.Offset = _DefaultOffset;
				}
				_Hidden = false;
			}
		}
	}
}
