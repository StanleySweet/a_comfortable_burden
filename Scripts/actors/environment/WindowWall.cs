using Godot;

namespace otter_ways.cm.actors.environment
{
	public class WindowWall : Actor
	{
		[Export] Texture _LoweredSprite;
		[Export] readonly bool _STARTING_LOWERED;
		[Export] NodePath _WindowSpritePath;

		Texture _DefaultSprite;
		Sprite _WindowSprite;

		public override void
		_EnterTree()
		{
			_WindowSprite = GetNode<Sprite>(_WindowSpritePath);
			base._EnterTree();
			_DefaultSprite = _Sprite.Texture;
			if (_STARTING_LOWERED)
			{
				Disappear();
			}
		}

		public override void
		Disappear()
		{
			_StayHidden = _Hidden ? true : false;
			_Hidden = true;
			if (!_StayHidden)
			{
				_Sprite.Texture = _LoweredSprite;
				_WindowSprite.Hide();
			}
		}

		public override void
		ResetSprite()
		{
			if (_StayHidden)
			{
				_StayHidden = false;
			}
			else
			{
				_Sprite.Texture = _DefaultSprite;
				_WindowSprite.Show();
				_Hidden = false;
			}
		}

		public override void 
		ReduceAlpha()
		{
			base.ReduceAlpha();
			if (!_StayTransparent)
			{
				_WindowSprite.Modulate = new Color(_WindowSprite.Modulate.r, _WindowSprite.Modulate.g,
												   _WindowSprite.Modulate.b, _REDUCED_ALPHA / 255f);
			}
		}

		public override void
		ResetAlpha()
		{
			if (_StayTransparent)
			{
				_StayTransparent = false;
			}
			else
			{
				_Transparent = false;
				_Sprite.Modulate = new Color(_Sprite.Modulate.r, _Sprite.Modulate.g,
											 _Sprite.Modulate.b, _DefaultAlpha);
				_WindowSprite.Modulate = new Color(_WindowSprite.Modulate.r, _WindowSprite.Modulate.g,
												   _WindowSprite.Modulate.b, _DefaultAlpha);
			}
		}
	}
}
