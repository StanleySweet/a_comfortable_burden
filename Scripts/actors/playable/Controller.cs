namespace otter_ways.cm.actors.playable
{
    public interface Controller
    {
        Existing Identity { get; }
        Godot.Vector2 Velocity { get; }
        Godot.Vector2 Position { get; set; }
        Godot.Vector2 GlobalPosition { get; set; }
        Godot.Vector2 CameraParameters { get; }

        void TransitionIn();
        void TransitionOut();
        void Move(float inputhorizontal, float inputvertical);
        void DoSomething();
    }
}
