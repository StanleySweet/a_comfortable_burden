using Godot;

namespace otter_ways.cm.actors.playable.character
{
	public class Controller : KinematicBody2D, playable.Controller
	{
		public static readonly Vector2 RIGHT = new Vector2(1f, 1f);
		public static readonly Vector2 LEFT = new Vector2(-1f, -1f);
		public static readonly Vector2 DOWN = new Vector2(-1f, 1f);
		public static readonly Vector2 UP = new Vector2(1f, -1f);

		[Export] systems.Player _Player;
		//[Export] systems.MentalLoad _MentalLoad;
		[Export] audio.PlayerSounds _Sounds;
		[Export] MovementAttributes _Movement;
		[Export] NodePath _AnimatorHandlerPath;
		[Export] NodePath _AudioPlayerPath;
		[Export] Existing _Identity;
		[Export] Vector2 _StartingDirection = new Vector2(1f, 1f);

		states.Disabled _CachedDisabled;
		states.Waiting _CachedWaiting;
		states.Idle _CachedIdle;
		states.Movement _CachedMovement;
		states.Action _CachedAction;

		states.State[] _StateHistory;
		int _CurrentHistoryIndex;

		float _InputHorizontal;
		float _InputVertical;

		AnimatorHandler _AnimatorHandler;
		audio.RandomPitchPlayer _AudioPlayer;

		Physics2DDirectSpaceState _SpaceState;

		public Vector2 _Velocity;
		public Vector2 FacingDirection;
		public AnimatorHandler Animator { get { return _AnimatorHandler; } }
		public audio.RandomPitchPlayer AudioPlayer { get { return _AudioPlayer; } }
		public audio.PlayerSounds Sounds { get { return _Sounds; } }
		public MovementAttributes Movement { get { return _Movement; } }
		//public systems.MentalLoad MentalLoad { get { return _MentalLoad; } }
		public Existing Identity { get { return _Identity; } }
		public Vector2 Velocity { get { return _Velocity; } }
		public Vector2 CameraParameters { get { return new Vector2(.9f, .9f); } }

		public override void
		_EnterTree()
		{
			_StateHistory = new states.State[4];
			_CachedDisabled = new states.Disabled(this);
			_CachedWaiting = new states.Waiting(this);
			_CachedIdle = new states.Idle(this);
			_CachedMovement = new states.Movement(this);
			_CachedAction = new states.Action(this);
			_StateHistory[0] = _CachedDisabled;

			CollisionLayer = cm.Layers.PLAYER;
			CollisionMask = cm.Layers.PLAYER | cm.Layers.STATIC;

			SetProcess(false);
			SetPhysicsProcess(false);
			_Player.AttachController(this);

			_Velocity = new Vector2();
			FacingDirection = _StartingDirection;
			_AnimatorHandler = GetNode<AnimatorHandler>(_AnimatorHandlerPath);
			_AudioPlayer = GetNode<audio.RandomPitchPlayer>(_AudioPlayerPath);
		}

		public override void 
		_Ready()
		{
			PushState(_CachedWaiting);
		}

		public override void 
		_ExitTree()
		{
			Destroy();
		}

		public override void
		_PhysicsProcess(float delta)
		{
			_SpaceState = GetWorld2d().DirectSpaceState;
			_StateHistory[_CurrentHistoryIndex].PhysicsProcess(delta, _SpaceState);
		}

		public void
		TransitionIn()
		{
			SetProcess(true);
			SetPhysicsProcess(true);
			PopState();
			PushState(_CachedIdle);
		}

		public void
		TransitionOut()
		{
			Hide();
			for (int i = _CurrentHistoryIndex; i > 0; i--)
			{
				PopState();
			}
			_AnimatorHandler.StopEverything();
			CollisionLayer = 0;
			CollisionMask = 0;
			_SpaceState = null;
			//_RayCastResults = null;
			SetProcess(false);
			SetPhysicsProcess(false);
			QueueFree();
		}

		public void
		Move(float inputhorizontal, float inputvertical)
		{
			_InputHorizontal = inputhorizontal;
			_InputVertical = inputvertical;
			_StateHistory[_CurrentHistoryIndex].Move(inputhorizontal, inputvertical);
		}

		public void
		DoSomething()
		{
			_StateHistory[_CurrentHistoryIndex].DoSomething();
		}
		
		public void
		PushAction()
		{
			_CachedAction.DoSomething();
			PushState(_CachedAction, false, false);
		}

		public void
		IdleToMovement()
		{
			PushState(_CachedMovement);
			Move(_InputHorizontal, _InputVertical);
		}

		public void
		MovementToIdle()
		{
			PopState();
		}

		public void
		ActionToAny()
		{
			PopState();
			Move(_InputHorizontal, _InputVertical);
		}

		public void
		ActionToAction(bool doaction)
		{
			PopState(false, false);

			if (doaction)
			{
				PushState(_CachedAction);
			}
		}

		public void
		OnTaskCompleted(int id)
		{
			ActionToAny();
		}

		public void
		ForceStatePhysics(float delta, Physics2DDirectSpaceState spacestate)
		{
			_StateHistory[_CurrentHistoryIndex].PhysicsProcess(delta, spacestate);
		}

		void
		PushState(states.State topush, bool enter = true, bool exit = true)
		{
			System.Diagnostics.Debug.Assert(_CurrentHistoryIndex < _StateHistory.Length, "Trying to push a state when the state array is full!");
			if (exit)
			{
				System.Diagnostics.Debug.Assert(_CurrentHistoryIndex > -1, "Trying to exit a state when the state array is empty!");
				_StateHistory[_CurrentHistoryIndex].Exit(topush.Type);
			}
			_StateHistory[++_CurrentHistoryIndex] = topush;
			if (enter)
			{
				_StateHistory[_CurrentHistoryIndex].Enter(_StateHistory[_CurrentHistoryIndex - 1].Type);
			}
		}

		void
		PopState(bool exit = true, bool enter = true)
		{
			System.Diagnostics.Debug.Assert(_CurrentHistoryIndex > -1, "Trying to pop a state when the state array is empty!");
			if (exit)
			{
				_StateHistory[_CurrentHistoryIndex].Exit(_StateHistory[_CurrentHistoryIndex - 1].Type);
			}
			_CurrentHistoryIndex--;
			if (enter)
			{
				_StateHistory[_CurrentHistoryIndex].Enter(_StateHistory[_CurrentHistoryIndex + 1].Type);
			}
		}

		public void
		Destroy()
		{
			for (int i = 0; i < _StateHistory.Length; i++)
			{
				_StateHistory[i] = null;
			}
			_CachedDisabled = null;
			_CachedIdle = null;
			_CachedMovement = null;
			_CachedAction = null;
		}
	}
}
