namespace otter_ways.cm.actors.playable.character.states
{
    public class Disabled : State
    {
		public override Existing Type { get { return Existing.Disabled; } }

        public
        Disabled(Controller controller) : base(controller)
        { }

        public override void
        PhysicsProcess(float delta, Godot.Physics2DDirectSpaceState spacestate)
        { }
    }
}
