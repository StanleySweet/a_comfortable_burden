namespace otter_ways.cm.actors.playable.character.states
{
    public abstract class State
    {
        protected Controller _Controller;
        public virtual Existing Type { get; }

        public
        State(Controller controller)
        {
            _Controller = controller;
        }

        public virtual void
        Enter(Existing previousstate)
        { }

        public abstract void
        PhysicsProcess(float delta, Godot.Physics2DDirectSpaceState spacestate);

        public virtual void
        Exit(Existing nextstate)
        { }

        public virtual void
        Move(float horizontal, float vertical)
        { }

        public virtual void
        DoSomething()
        { }
    }
}
