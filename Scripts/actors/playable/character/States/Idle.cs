namespace otter_ways.cm.actors.playable.character.states
{
	public class Idle : State
	{
		public override Existing Type { get { return Existing.Idle; } }

		public
		Idle(Controller controller) : base(controller)
		{ }

		public override void
		Enter(Existing previousstate)
		{
			if (_Controller.FacingDirection == Controller.UP)
			{
				_Controller.Animator.PlayIdleUp();
			}
			else if (_Controller.FacingDirection == Controller.RIGHT)
			{
				_Controller.Animator.PlayIdleRight();
			}
			else if (_Controller.FacingDirection == Controller.DOWN)
			{
				_Controller.Animator.PlayIdleDown();
			}
			else if (_Controller.FacingDirection == Controller.LEFT)
			{
				_Controller.Animator.PlayIdleLeft();
			}
		}

		public override void
		PhysicsProcess(float delta, Godot.Physics2DDirectSpaceState spacestate)
		{ }

		public override void
		Move(float horizontal, float vertical)
		{
            if (horizontal != 0f || vertical != 0f)
            {
                _Controller.IdleToMovement();
            }
        }

		public override void
		DoSomething()
		{
			_Controller.PushAction();
		}
	}
}
