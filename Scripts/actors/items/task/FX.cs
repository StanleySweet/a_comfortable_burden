using Godot;

namespace otter_ways.cm.actors.items.task
{
	public class FX : Node2D
	{
		[Export] PackedScene _FXWateringScene;
		[Export] PackedScene _FXCleaningScene;
		[Export] PackedScene _FXMopingScene;
		[Export] readonly FXSize _FX_SIZE;

		AnimatedSprite _FXInstance;

		public void
		Start()
		{
			_FXInstance = (AnimatedSprite)_FXCleaningScene.Instance();
			AddChild(_FXInstance);
			switch (_FX_SIZE)
			{
				case FXSize.Big:
					_FXInstance.Play("Big");
					break;
				case FXSize.Medium:
					_FXInstance.Play("Medium");
					break;
				case FXSize.Small:
					_FXInstance.Play("Small");
					break;
			}
		}

		public void
		Stop()
		{
			_FXInstance.Stop();
			_FXInstance.QueueFree();
			_FXInstance = null;
		}
	}
}
