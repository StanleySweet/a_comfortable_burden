using Godot;

namespace otter_ways.cm.actors.items.task
{
    public interface thing
    {
        string TextToDisplay { get; }
        Type Type { get; }
        bool CanBeDone { get; }

        Error Connect(string signal, Object target, string method, Godot.Collections.Array binds = null, uint flags = 0);
        void Disconnect(string signal, Object target, string method);
        void StartCompletion();
    }
}
