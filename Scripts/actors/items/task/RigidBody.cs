using Godot;

namespace otter_ways.cm.actors.items.task
{
	public class RigidBody : StaticBody2D
	{
		thing _Task;

		public thing Parent { get { return _Task; } }

		public void
		Initialise(thing parent, bool nocollisions)
		{
			_Task = parent;
			if (nocollisions)
			{
				CollisionLayer = 0;
				CollisionMask = 0;
			}
		}

		public void
		EnableCollisions()
		{
			CollisionLayer = cm.Layers.TASK;
			CollisionMask = cm.Layers.PLAYER;
		}

		public void
		DisableCollisions()
		{
			CollisionLayer = 0;
			CollisionMask = 0;
		}
	}
}
