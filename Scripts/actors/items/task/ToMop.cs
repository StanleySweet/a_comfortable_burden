using Godot;

namespace otter_ways.cm.actors.items.task
{
	public class ToMop : Actor, thing
	{
		[Signal] public delegate void Completed(int id);
		[Signal] public delegate void Revealed(int id);

		[Export] systems.TaskList _TaskList;
		[Export] NodePath _RigidBodyPath;
		[Export] NodePath _OutlinePath;
		[Export] NodePath _FXPath;
		[Export] readonly Type _TYPE;
		[Export] readonly float _DURATION = 2f;
		[Export] readonly bool _START_REVEALED;
		[Export] readonly bool _START_HIDDEN;

		public string TextToDisplay { get { return ""; } }
		public Type Type { get { return _TYPE; } }
		public bool StartRevealed { get { return _START_REVEALED; } }
		public bool CanBeDone { get { return !_Completed; } }

		FX _FX;
		Outline _Outline;
		RigidBody _RigidBody;
		float _CompletionCurrentTime;
		int _ID;
		bool _Completed;

		public override void
		_EnterTree()
		{
			_RigidBody = GetNode<RigidBody>(_RigidBodyPath);
			_Outline = GetNode<Outline>(_OutlinePath);
			_FX = GetNode<FX>(_FXPath);
			_ID = _TaskList.Register(this);
			base._EnterTree();
			if (_START_HIDDEN)
			{
				Disappear();
			}
		}

		public override void
		_Ready()
		{
			_RigidBody.Initialise(this, false);
			_Outline.Initialise(_START_REVEALED);
			SetPhysicsProcess(false);
		}

		public override void
		_PhysicsProcess(float delta)
		{
			_CompletionCurrentTime += delta;
			if (_CompletionCurrentTime >= _DURATION)
			{
				SetPhysicsProcess(false);
				Complete();
			}
		}

		void
		OnOutlineRevealed()
		{
			EmitSignal(nameof(Revealed), _ID);
		}

		public void
		StartCompletion()
		{
			SetPhysicsProcess(true);
			_Outline.Hide();
			_FX.Start();
		}

		void
		Complete()
		{
			_Completed = true;

			_Sprite.Texture = null;
			_FX.Stop();

			EmitSignal(nameof(Completed), _ID);
		}

		public override void
		ReduceAlpha()
		{
			base.ReduceAlpha();
			if (!_StayTransparent)
			{
				_Outline.Hide();
			}
		}

		public override void
		ResetAlpha()
		{
			if (_StayTransparent)
			{
				_StayTransparent = false;
			}
			else
			{
				_Transparent = false;
				_Sprite.Modulate = new Color(_Sprite.Modulate.r, _Sprite.Modulate.g,
											 _Sprite.Modulate.b, _DefaultAlpha);
				if (CanBeDone)
				{
					_Outline.Show();
				}
			}
		}
	}
}
