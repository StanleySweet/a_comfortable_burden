namespace otter_ways.cm.actors.items
{
    public enum FXSize
    {
        Big,
        Medium,
        Small
    };
}