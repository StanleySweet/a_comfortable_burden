using Godot;

namespace otter_ways.cm.audio
{
	public class PlayerSounds : Resource
	{
		[Export] readonly AudioStream[] _MOPING_SOUNDS;
		public AudioStream[] MOPING_SOUNDS { get { return _MOPING_SOUNDS; } }
		[Export] readonly AudioStream[] _WATERING_SOUNDS;
		public AudioStream[] WATERING_SOUNDS { get { return _WATERING_SOUNDS; } }
		[Export] readonly AudioStream[] _CLEANING_SOUNDS;
		public AudioStream[] CLEANING_SOUNDS { get { return _CLEANING_SOUNDS; } }
	}
}
