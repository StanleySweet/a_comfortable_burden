using Godot;

namespace otter_ways.cm.audio
{
    public class RandomPitchPlayer : AudioStreamPlayer
    {
        const string _FINISHED = "finished";
        const float _NO_PITCH_VARIATION = 1f;

        [Export] float _DefaultPitchVariation = 1.1f;

        AudioStreamRandomPitch _ASRP;

        public override void
        _EnterTree()
        {
            //Assert that
            _ASRP = Stream as AudioStreamRandomPitch;
            _ASRP.RandomPitch = _DefaultPitchVariation;
        }

        public void
        Play(AudioStream toplay, bool varypitch = true, float fromposition = 0f)
        {
            _ASRP.RandomPitch = !varypitch ? _NO_PITCH_VARIATION : _DefaultPitchVariation;
            _ASRP.AudioStream = toplay;
            Play(fromposition);
            if (!IsConnected(_FINISHED, this, nameof(OnSoundPlayingFinished)))
            {
                _ = Connect(_FINISHED, this, nameof(OnSoundPlayingFinished));
            }
        }

        public void
        Play(AudioStream toplay, float pitchvariation, float fromposition = 0f)
        {
            _ASRP.RandomPitch = pitchvariation;
            _ASRP.AudioStream = toplay;
            Play(fromposition);
            if (!IsConnected(_FINISHED, this, nameof(OnSoundPlayingFinished)))
            {
                _ = Connect(_FINISHED, this, nameof(OnSoundPlayingFinished));
            }
        }

        protected virtual void
        OnSoundPlayingFinished()
        {
            _ASRP.RandomPitch = _DefaultPitchVariation;
            Disconnect(_FINISHED, this, nameof(OnSoundPlayingFinished));
        }
    }
}
